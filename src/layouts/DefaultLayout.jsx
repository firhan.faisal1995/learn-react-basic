import { Container } from "@mui/material";
import Navbar from "../Navbar";
import { useLocation } from "react-router-dom";
import Footer from "../Footer";

/* eslint-disable react/prop-types */
export default function DefaultLayout({ children }) {
  const location = useLocation();

  // set title
  document.title = "DLanguage " + location.pathname.replace("/", "") || "Home";
  return (
    <>
      <Navbar />
      <Container
        sx={{
          marginTop: "100px",
        }}
      >
        {children}
      </Container>
      <Footer />
    </>
  );
}
