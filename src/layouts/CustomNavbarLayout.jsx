import { Container } from "@mui/material";

/* eslint-disable react/prop-types */
export default function CustomNavbarLayout({ children }) {
  return <Container>{children}</Container>;
}
