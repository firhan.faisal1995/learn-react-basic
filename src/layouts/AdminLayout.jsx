import PermanentDrawerLeft from "../components/admin/Sidebar";

/* eslint-disable react/prop-types */
export default function AdminLayout({ children }) {
  return <PermanentDrawerLeft>{children}</PermanentDrawerLeft>;
}
