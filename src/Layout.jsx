import styled from "styled-components";

const Header = styled.div`
  grid-area: header;
  background: #f2f2f2;
  padding: 10px;
`;

const Menu = styled.div`
  grid-area: menu;
  background: #f2f2f2;
  padding: 10px;
`;

const Main = styled.div`
  grid-area: main;
  background: #ffffff;
  padding: 10px;
`;

const Right = styled.div`
  grid-area: right;
  background: #ffffff;
  padding: 10px;
`;

const Footer = styled.div`
  grid-area: footer;
  background: #303030;
  padding: 10px;
`;

const LayoutTemplate = styled.div`
  display: grid;
  grid-template-areas:
    "header header header header header header"
    "menu main main main right right"
    "menu footer footer footer footer footer";
`;

const Layout = () => {
  return (
    <LayoutTemplate>
      <Header>Header</Header>
      <Menu>Menu</Menu>
      <Main>Main</Main>
      <Right>Right</Right>
      <Footer>Footer</Footer>
    </LayoutTemplate>
  );
};

export default Layout;
