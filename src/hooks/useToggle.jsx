import { useState } from "react";

function useToggle() {
  const [toggle, setToggle] = useState(false);

  const toggleFunc = () => {
    setToggle(!toggle);
    // if TRUE then FALSE
    // if FALSE then TRUE
  };

  return {
    isToggle: toggle,
    toggle: toggleFunc,
  };
}

export default useToggle;
