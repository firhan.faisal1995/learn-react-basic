import { useState } from "react";

export default function useTodo() {
  // this will contains all todo
  const [currentTodo, setCurrentTodo] = useState("");
  const [todos, setTodos] = useState([]);

  const addTodo = (todo) => {
    setTodos([...todos, todo]);
  };

  const removeTodo = (todo) => {
    setTodos(todos.filter((item) => item !== todo));
  };

  return {
    currentTodo,
    setCurrentTodo,
    todos,
    addTodo,
    removeTodo,
  };
}
