import axios from "axios";
import { useEffect, useState } from "react";

// pengen tau lagi loading atau ngga
// pengen tau data nya apa
export default function useGetProducts() {
  const [loading, setLoading] = useState(true);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    async function getProducts() {
      setLoading(true);
      const res = await axios(
        "http://52.237.194.35:2024/api/Type/GetActiveType"
      );
      setLoading(false);
      const { data, status } = res;

      if (status !== 200) return;

      setProducts(data);
    }

    getProducts();
  }, []);

  return { loading, products };
}
