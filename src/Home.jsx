import { useEffect } from "react";
import Product from "./Product";

const Home = () => {
  useEffect(() => {
    // on first render
    console.log("first render only");
  }, []);

  return (
    <div>
      <h1>Home</h1>
      <div
        style={{
          display: "flex",
          gap: 20,
          justifyContent: "space-between",
        }}
      >
        <Product name="Product 1" price={1000} description="Description 1" />
        <Product name="Product 2" price={2000} description="Description 2" />
        <Product name="Product 3" price={3000} description="Description 3" />
      </div>
    </div>
  );
};

export default Home;
