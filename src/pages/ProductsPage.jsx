import { Link, useSearchParams } from "react-router-dom";

export default function ProductsPage() {
  const [searchParams] = useSearchParams();
  const keyword = searchParams.get("keyword");
  const page = searchParams.get("page");

  return (
    <>
      <h1>Products Page</h1>
      <p>search: {keyword}</p>
      <p>page: {page} of 200</p>
      <ul>
        <li>
          <Link to="/products/laptop">Laptop</Link>
        </li>
        <li>
          <Link to="/products/sepatu">Sepatu</Link>
        </li>
        <li>
          <Link to="/products/tas">Tas</Link>
        </li>
        <li>
          <Link to="/products/gantungan-kunci">Gantungan Kunci</Link>
        </li>
      </ul>
    </>
  );
}
