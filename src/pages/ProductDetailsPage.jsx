import { useParams } from "react-router-dom";

export default function ProductDetailsPage() {
  const { slug, id, from } = useParams();

  return (
    <h1>
      Details For {slug} {id} {from}
    </h1>
  );
}
