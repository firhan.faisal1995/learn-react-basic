import { CircularProgress } from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export default function DetailCategoryPage() {
  const { name } = useParams();

  const [details, setDetails] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function getDetails() {
      setLoading(true);
      const res = await axios(
        `http://52.237.194.35:2024/api/Type/GetTypeByName?name=${name}`
      );
      setDetails(res.data);
      setLoading(false);
    }

    getDetails();
  }, [name]);

  if (loading) return <CircularProgress />;

  if (!details) return <></>;

  const { description, image: base64Image } = details;

  return (
    <>
      <img src={`data:image/png;base64,${base64Image}`} width="100%" />
      <h1>{description}</h1>
    </>
  );
}
