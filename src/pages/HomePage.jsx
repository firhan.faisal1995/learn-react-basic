import useTodo from "../hooks/useTodo";

export default function HomePage() {
  const { currentTodo, setCurrentTodo, todos, addTodo, removeTodo } = useTodo();

  return (
    <>
      <h1>Home Page</h1>
      <input
        type="text"
        value={currentTodo}
        onChange={(e) => setCurrentTodo(e.target.value)}
      />
      <button
        onClick={() => {
          addTodo(currentTodo);
          setCurrentTodo("");
        }}
      >
        Add Todo
      </button>
      <ul>
        {todos.map((todo) => (
          <li key={todo}>
            {todo}{" "}
            <button type="button" onClick={() => removeTodo(todo)}>
              remove
            </button>
          </li>
        ))}
      </ul>
    </>
  );
}
