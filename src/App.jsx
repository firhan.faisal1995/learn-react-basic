import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import CartPage from "./pages/CartPage";
import ProductsPage from "./pages/ProductsPage";
import DefaultLayout from "./layouts/DefaultLayout";
import Confirmed from "./pages/Confirmed";
import CustomNavbarLayout from "./layouts/CustomNavbarLayout";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";
import ScrollToTop from "./components/Todo/ScrollToTop";
import ProductDetailsPage from "./pages/ProductDetailsPage";
import DetailCategoryPage from "./pages/DetailCategoryPage";
import Dashboard from "./pages/admin/Dashboard";
import AdminLayout from "./layouts/AdminLayout";
import ProductList from "./pages/admin/ProductList";
import CategoriesList from "./pages/admin/CategoriesList";
import UserList from "./pages/admin/UserList";
import CategoryCreate from "./pages/admin/CategoryCreate";

const App = () => {
  return (
    <BrowserRouter>
      <ScrollToTop />
      <Routes>
        <Route
          path="/"
          element={
            <DefaultLayout>
              <HomePage />
            </DefaultLayout>
          }
        />
        <Route
          path="/category/:name"
          element={
            <DefaultLayout>
              <DetailCategoryPage />
            </DefaultLayout>
          }
        />
        <Route
          path="/login"
          element={
            <DefaultLayout>
              <LoginPage />
            </DefaultLayout>
          }
        />
        <Route
          path="/register"
          element={
            <DefaultLayout>
              <RegisterPage />
            </DefaultLayout>
          }
        />
        <Route
          path="/cart"
          element={
            <DefaultLayout>
              <CartPage />
            </DefaultLayout>
          }
        />
        <Route
          path="/products"
          element={
            <DefaultLayout>
              <ProductsPage />
            </DefaultLayout>
          }
        />
        <Route
          path="/products/:slug/:id/:from" // const { slug } = useParams()
          element={
            <DefaultLayout>
              <ProductDetailsPage />
            </DefaultLayout>
          }
        />
        <Route
          path="/confirmed"
          element={
            <CustomNavbarLayout>
              <Confirmed />
            </CustomNavbarLayout>
          }
        />

        <Route path="/admin">
          <Route
            path=""
            element={
              <AdminLayout>
                <Dashboard />
              </AdminLayout>
            }
          />
          <Route
            path="categories"
            element={
              <AdminLayout>
                <CategoriesList />
              </AdminLayout>
            }
          />
          <Route
            path="categories/create"
            element={
              <AdminLayout>
                <CategoryCreate />
              </AdminLayout>
            }
          />
          <Route
            path="products"
            element={
              <AdminLayout>
                <ProductList />
              </AdminLayout>
            }
          />
          <Route
            path="users"
            element={
              <AdminLayout>
                <UserList />
              </AdminLayout>
            }
          />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
