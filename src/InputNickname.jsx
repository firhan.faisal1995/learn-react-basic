// create me component that has input field and button
// when button is clicked, it will change the value of the state

import { useEffect, useState } from "react";
import styled from "styled-components";
import "./input_nickname.css";

const MyButton = styled.button`
  padding: 5px 40px;
  background: ${(props) => props.color || "white"};
  border-radius: 5px;
  margin-top: 10px;

  // add transition
  transition: 0.3s;

  cursor: pointer;

  &:hover {
    box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.5);
  }
`;

const InputNickname = () => {
  const [nickname, setNickname] = useState("");
  const [password, setPassword] = useState("");
  const [passwordError, setPasswordError] = useState("");
  const [recommendations, setRecommendations] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    setTimeout(() => {
      setRecommendations(["a", "b", "c"]);
      setLoading(false);
    }, 2000);
  }, []); // only run once

  useEffect(() => {
    // code di dalam sini akan jalan ketika ada perubahan state nickname
    console.log("nickname changed");
  }, [nickname]);

  useEffect(() => {
    console.log("validating password");
    if (password.length < 6 || !/^(?=.*[a-zA-Z])(?=.*[0-9])/.test(password)) {
      setPasswordError(
        "Password must be at least 6 characters long and include alphanumeric characters."
      );
    } else {
      setPasswordError("");
    }
  }, [password]); // dependency

  return (
    <div>
      nickname:{nickname}
      <br />
      <input
        type="text"
        value={nickname}
        onChange={(e) => setNickname(e.target.value)}
      />
      {loading ? (
        <div>Loading...</div>
      ) : (
        <ul>
          {recommendations.map((recommendation) => (
            <li key={recommendation}>{recommendation}</li>
          ))}
        </ul>
      )}
      <br />
      <input
        style={{
          height: "30px",
          borderRadius: "5px",
          border: passwordError !== "" ? "1px solid red" : "1px solid black",
        }}
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <br />
      {passwordError}
      <MyButton>Submit</MyButton>
      <button className="my-button">Test</button>
    </div>
  );
};

export default InputNickname;
