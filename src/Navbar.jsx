/* eslint-disable react/prop-types */
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { Stack } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "./contexts/AuthContext";

export default function Navbar({ title = "Dlanguage" }) {
  const navigate = useNavigate();

  const { isLoggedIn } = useContext(AuthContext);

  const UnauthorizedComponent = () => {
    return (
      <>
        <Button
          // onClick={() => navigate("/login")}
          color="success"
          variant="contained"
          onClick={() => navigate("/login")}
        >
          Login
        </Button>
        <Button
          onClick={() => navigate("/register")}
          color="warning"
          variant="contained"
        >
          Sign Up
        </Button>
      </>
    );
  };

  const AuthorizedComponent = () => {
    return (
      <>
        <Typography>My Profile</Typography>
        <Button
          onClick={() => navigate("/products")}
          color="success"
          variant="contained"
        >
          Products
        </Button>
        <Button
          onClick={() => navigate("/cart")}
          color="success"
          variant="contained"
        >
          Cart
        </Button>
        <Button color="warning" variant="contained" onClick={() => {}}>
          Logout
        </Button>
      </>
    );
  };

  return (
    <Box sx={{ flexGrow: 1 }} style={{}}>
      <AppBar
        position="fixed"
        style={{
          padding: "0px",
        }}
      >
        <Toolbar
          style={{
            backgroundColor: "#fff",
            color: "#000000",
          }}
        >
          <Stack
            onClick={() => navigate("/")}
            direction="row"
            gap={1}
            flexGrow={1}
            alignItems="center"
          >
            <img src="/images/logo.svg" />
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
              {title}
            </Typography>
          </Stack>

          <Stack direction="row" gap={2}>
            {isLoggedIn ? <AuthorizedComponent /> : <UnauthorizedComponent />}
          </Stack>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
