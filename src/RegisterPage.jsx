import { TextField, Button, Stack, Box } from "@mui/material";
import { useFormik } from "formik";
import * as Yup from "yup";

const RegisterPage = () => {
  const formik = useFormik({
    initialValues: {
      email: "",
      firstName: "",
      lastName: "",
      password: "",
    },
    validationSchema: Yup.object({
      email: Yup.string().email("Invalid email address").required("Required"),
      firstName: Yup.string().required("Required"),
      lastName: Yup.string().required("Required"),
      password: Yup.string().required("Required").min(8, "Too Short!"),
    }),
    validateOnMount: true,
  });

  const handleRegister = () => {
    // Handle registration logic here
  };

  return (
    <>
      <h1>Register</h1>
      <Stack direction="column" gap={2}>
        <TextField
          label="Email"
          name="email"
          value={formik.values.email}
          onChange={formik.handleChange}
          error={formik.errors.email}
          helperText={formik.errors.email}
        />
        <TextField
          label="First Name"
          name="firstName"
          value={formik.values.firstName}
          onChange={formik.handleChange}
        />
        <TextField
          label="Last Name"
          name="lastName"
          value={formik.values.lastName}
          onChange={formik.handleChange}
        />
        <TextField
          label="Password"
          name="password"
          value={formik.values.password}
          onChange={formik.handleChange}
        />
        <Box textAlign="end">
          <Button variant="contained" color="primary" onClick={handleRegister}>
            Register
          </Button>
        </Box>
      </Stack>
      <div
        style={{
          height: "2000px",
          background: "blue",
        }}
      ></div>
    </>
  );
};

export default RegisterPage;
