import PropTypes from "prop-types";

const Button = ({ testArray, label, color, height, width }) => {
  return (
    <button
      style={{
        backgroundColor: color,
        height,
        width,
      }}
    >
      {label}
      {testArray.map((letter, index) => (
        <span key={index}>{letter}</span>
      ))}
    </button>
  );
};

Button.propTypes = {
  testArray: PropTypes.arrayOf(PropTypes.string),
  label: PropTypes.string.isRequired,
  color: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
};

export default Button;
