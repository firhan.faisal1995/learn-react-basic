import { Button, Stack, TextField } from "@mui/material";
import styled from "styled-components";
import { useContext, useState } from "react";
import axios from "axios";
import { AuthContext } from "./contexts/AuthContext";

const Title = styled.div`
  /* Welcome Back! */

  width: 190px;
  height: 29px;

  font-family: "Montserrat";
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 29px;

  /* Primary */
  color: #226957;

  margin-bottom: 16px;
`;

const Subtitle = styled.div`
  /* Please login first */

  width: 132px;
  height: 20px;

  font-weight: 400;
  font-size: 16px;
  line-height: 20px;
  /* identical to box height */

  /* Gray 2 */
  color: #4f4f4f;

  margin-bottom: 32px;
`;

const LoginPage = () => {
  const [email, setEmail] = useState("atuny0");
  const [password, setPassword] = useState("9uQFF1Lh");

  const { setIsLoggedIn } = useContext(AuthContext);

  /**
   *
   * @param {string} password
   * @returns {string} error message
   */
  const validatePassword = () => {
    if (password.length < 8) {
      return "Password kurang dari 8 karakter";
    } else {
      return "";
    }
  };

  const passwordError = validatePassword();

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const res = await axios.post("https://dummyjson.com/auth/login", {
        username: email,
        password,
      });
      console.log(res);
      setIsLoggedIn(true);
      alert("success");
    } catch (err) {
      alert(err);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <Title>Welcome back</Title>
      <Subtitle>Please login first</Subtitle>
      <Stack gap={1}>
        <TextField
          fullWidth
          id="outlined-basic"
          label="Email"
          variant="outlined"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <br />
        <TextField
          fullWidth
          id="outlined-basic"
          label="Password"
          variant="outlined"
          value={password}
          onChange={(event) => {
            setPassword(event.target.value);
          }}
          error={!!passwordError}
          helperText={passwordError}
        />
        <div
          style={{
            marginTop: "10px",
          }}
        >
          Forgot password? <a href="#">Click here</a>
        </div>
        <div
          style={{
            marginTop: "10px",
            textAlign: "end",
          }}
        >
          <Button type="submit" variant="contained" color="success">
            Login
          </Button>
        </div>
      </Stack>
    </form>
  );
};

export default LoginPage;
