import PropTypes from "prop-types";
import { Button, TextField } from "@mui/material";
import { useState } from "react";

export default function TodoForm({ addTodo }) {
  const [todoText, setTodoText] = useState("");

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        addTodo(todoText);
        setTodoText("");
      }}
    >
      <TextField
        value={todoText}
        onChange={(e) => setTodoText(e.target.value)}
        size="small"
        label="What to do"
        type="text"
      />
      <Button type="submit" variant="contained" color="primary">
        Add
      </Button>
    </form>
  );
}

TodoForm.propTypes = {
  addTodo: PropTypes.func.isRequired,
};
