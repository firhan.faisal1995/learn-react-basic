import PropTypes from "prop-types";
import { TextField } from "@mui/material";

const SearchBox = ({ search, setSearch }) => {
  return (
    <div>
      <TextField
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        size="small"
        label="Search"
        type="text"
      />
    </div>
  );
};

SearchBox.propTypes = {
  search: PropTypes.string.isRequired,
  setSearch: PropTypes.func.isRequired,
};

export default SearchBox;
