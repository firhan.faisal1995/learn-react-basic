/* eslint-disable react/prop-types */

import Todo from "./Todo";

const TodoList = ({ todos, toggleTodo }) => {
  return (
    <ul>
      {todos.map((todo) => {
        return <Todo key={todo.id} todo={todo} toggleTodo={toggleTodo} />;
      })}
    </ul>
  );
};

export default TodoList;
