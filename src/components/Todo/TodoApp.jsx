import { Container } from "@mui/material";
import { useState } from "react";
import TodoList from "./TodoList";
import TodoForm from "./TodoForm";
import SearchBox from "./SearchBox";

const TodoApp = () => {
  const [todos, setTodos] = useState([]);
  const [search, setSearch] = useState("");

  const addTodo = (text) => {
    const newTodos = [...todos, { id: todos.length, text, isCompleted: false }];
    setTodos(newTodos);
  };

  const toggleTodo = (id) => {
    const newTodos = todos.map((todo) => {
      if (todo.id === id) {
        return { ...todo, isCompleted: !todo.isCompleted };
      }
      return todo;
    });
    setTodos(newTodos);
  };

  const filterTodos = todos.filter((todo) => {
    return todo.text.toLowerCase().includes(search.toLowerCase());
  });

  return (
    <Container
      sx={{
        padding: "10px",
      }}
    >
      <TodoForm addTodo={addTodo} />
      <SearchBox search={search} setSearch={setSearch} />
      <TodoList todos={filterTodos} toggleTodo={toggleTodo} />
    </Container>
  );
};

export default TodoApp;
