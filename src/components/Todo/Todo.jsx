/* eslint-disable react/prop-types */
export default function Todo({ todo, toggleTodo }) {
  const { id, text, isCompleted } = todo;

  return (
    <li
      onClick={() => toggleTodo(id)}
      style={{
        textDecoration: isCompleted ? "line-through" : "",
      }}
    >
      {isCompleted ? "✅" : "⬜"}
      {text}
    </li>
  );
}
