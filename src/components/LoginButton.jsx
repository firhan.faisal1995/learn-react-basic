import { Button } from "@mui/material";
import { useContext } from "react";
import { AuthContext } from "../contexts/AuthContext";

export default function LoginButton() {
  const { setIsLoggedIn } = useContext(AuthContext);

  return (
    <Button variant="contained" onClick={() => setIsLoggedIn(true)}>
      Login
    </Button>
  );
}
