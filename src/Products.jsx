import { useEffect, useState } from "react";
import Product from "./Product";
import { CircularProgress, Grid } from "@mui/material";
import axios from "axios";

export default function Products() {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  // get all products when component first mounted
  useEffect(() => {
    // fetch itu Promise, baru akan selesai di eksekusi ketika ada respon dari server

    // then styled
    // fetch("https://dummyjson.com/products")
    //   .then((res) => res.json())
    //   .then((data) => console.log(data));

    // await styled
    async function getProducts() {
      console.log("get products mulai"); // 1

      //   const res = await fetch("https://dummyjson.com/products");
      //   const data = await res.json();

      setLoading(true);
      const res = await axios(
        "http://52.237.194.35:2024/api/Type/GetActiveType"
      );
      setLoading(false);
      const { data, status } = res;

      if (status !== 200) return;

      setProducts(data);

      console.log("get products selesai"); // 2
    }

    getProducts();
  }, [setProducts]);

  if (loading) {
    return <CircularProgress />;
  }

  return (
    <Grid container spacing={3}>
      {products.map((product, key) => (
        <Grid item key={key} xs={6} md={4} lg={3}>
          <Product product={product} />
        </Grid>
      ))}
    </Grid>
  );
}
