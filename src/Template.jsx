/* eslint-disable react/prop-types */
import { Container, ThemeProvider, createTheme } from "@mui/material";
import Navbar from "./Navbar";

const myTheme = createTheme({
  typography: {
    fontFamily: "Montserrat",
  },
  palette: {
    success: {
      main: "#226957",
    },
    warning: {
      main: "#EA9E1F",
      contrastText: "#fff",
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          borderRadius: "8px",
          textTransform: "none",
          padding: "10px 20px",
          boxShadow: "none",
        },
      },
    },
  },
});

const Template = ({ children }) => {
  return (
    <ThemeProvider theme={myTheme}>
      <Navbar />
      <Container maxWidth="md">{children}</Container>
    </ThemeProvider>
  );
};

export default Template;
