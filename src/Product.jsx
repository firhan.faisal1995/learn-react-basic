import { Link } from "react-router-dom";

/* eslint-disable react/prop-types */
const Product = ({ product }) => {
  const { type_name, description, image } = product; // destructuring

  return (
    <Link to={`category/${type_name}`}>
      <img
        src={`data:image/png;base64,${image}`}
        alt={type_name}
        width="100%"
      />
      <div>
        <b>{type_name}</b>
      </div>
      <i>{description}</i>
    </Link>
  );
};

export default Product;
